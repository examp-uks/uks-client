// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

import { getStorage } from "firebase/storage";

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyDDVkj5cYm5TMihzbCIkHkVGOWVszSDfxk",
  authDomain: "examprofile-e11bd.firebaseapp.com",
  projectId: "examprofile-e11bd",
  storageBucket: "examprofile-e11bd.appspot.com",
  messagingSenderId: "269042294691",
  appId: "1:269042294691:web:55195775d4da98e9f48416",
  measurementId: "G-PDPVGQD8HH",
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

// Initialize Cloud Storage and get a reference to the service
export const storage = getStorage(app);

const analytics = getAnalytics(app);
