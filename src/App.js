import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import "./App.css";
import RegisterPage from "./pages/RegisterPage";
import LoginPage from "./pages/LoginPage";
import Welcome from "./pages/Welcome";
import DaftarObat from "./pages/DaftarObat";
import AddObat from "./component/AddObat";
import Dashboard from "./pages/Dashboard";
import Tindakan from "./pages/Tindakan";
import Penanganan from "./pages/Penanganan";
import Diagnosa from "./pages/Diagnosa";
import DataGuru from "./pages/DataGuru";
import DataSiswa from "./pages/DataSiswa";
import DataKaryawan from "./pages/DataKaryawan";
import EditDiagnosa from "./edit/EditDiagnosa";
import EditObat from "./edit/EditObat";
import PeriksaPasien from "./pages/PeriksaPasien";
import Tangani from "./pages/Tangani";
import EditTindakan from "./edit/EditTindakan";
import EditPenanganan from "./edit/EditPenanganan";
import EditGuru from "./edit/EditGuru";
import EditSiswa from "./edit/EditSiswa";
import EditKaryawan from "./edit/EditKaryawan";
import EditProfile from "./edit/EditProfile";

function App() {
  return (
    <Router>
      <div className="container">
        <div className="main">
          <Routes>
            <Route path="/Register" element={<RegisterPage />} />
            <Route path="/login" element={<LoginPage />} />
            <Route path="/" element={<Welcome />} />
            <Route path="/obat" element={<DaftarObat />} />
            <Route path="/dashboard" element={<Dashboard />} />
            <Route path="/periksaPasien" element={<PeriksaPasien />} />
            <Route path="/tindakan" element={<Tindakan />} />
            <Route path="/penanganan" element={<Penanganan />} />
            <Route path="/diagnosa" element={<Diagnosa />} />
            <Route path="/guru" element={<DataGuru />} />
            <Route path="/siswa" element={<DataSiswa />} />
            <Route path="/tangani/:id" element={<Tangani />} />
            <Route path="/karyawan" element={<DataKaryawan />} />
            <Route path="/updatePenyakit/:id" element={<EditDiagnosa />} />
            <Route path="/updateObat/:id" element={<EditObat />} />
            <Route path="/updateTindakan/:id" element={<EditTindakan />} />
            <Route path="/updatePenanganan/:id" element={<EditPenanganan />} />
            <Route path="/updateGuru/:id" element={<EditGuru />} />
            <Route path="/updateSiswa/:id" element={<EditSiswa />} />
            <Route path="/updateKaryawan/:id" element={<EditKaryawan />} />
            <Route path="/user/:id" element={<EditProfile />} />
          </Routes>
        </div>
      </div>
    </Router>
  );
}

export default App;
