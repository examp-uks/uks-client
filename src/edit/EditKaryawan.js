import React, { useEffect, useState } from "react";
import Sidebar from "../component/Sidebar";
import { useNavigate, useParams } from "react-router-dom";
import Swal from "sweetalert2";
import { instance as axios } from "../util/Api";

function EditKaryawan() {
  const navigate = useNavigate();
  const { id } = useParams();

  //   Deklarasi variabel
  const [nama, setNama] = useState("");
  const [tempat, setTempat] = useState("");
  const [tanggal, setTanggal] = useState("");
  const [alamat, setAlamat] = useState("");

  //   Method update
  const updateKaryawan = async () => {
    try {
      // untuk mengirimkan file multipart ke server
      const data = {
        nama: nama,
        tempat: tempat,
        tanggal: tanggal,
        alamat: alamat,
      };

      Swal.fire({
        title: "Apakah Anda ingin menyimpan perubahan?",
        icon: "question",
        showDenyButton: true,
        confirmButtonText: "Save",
        denyButtonText: `Cancel`,
      }).then((result) => {
        if (result.isConfirmed) {
          console.log(data);
          axios.put(`karyawan/data/update/${id}`, data);

          axios.put(`karyawan/data/update/${id}`, data);

          navigate("/tindakan");
          Swal.fire({
            icon: "success",
            title: "Saved!",
            text: "Menu berhasil diubah!",
            showConfirmButton: false,
            timer: 1000,
          });
        } else if (result.isDenied) {
          Swal.fire({
            icon: "info",
            title: "",
            text: "Perubahan tidak disimpan!",
            showConfirmButton: false,
            timer: 1000,
          });
        }
      });
    } catch (err) {
      console.log(err);
    }
  };

  //   Method save
  const save = () => {
    updateKaryawan();
  };

  //   Method get by id
  const getById = async () => {
    const { data } = await axios.get(`karyawan/data/${id}`);

    setNama(data.nama);
    setTempat(data.tempat);
    setTanggal(data.tanggal);
    setAlamat(data.alamat);
  };

  useEffect(() => {
    getById();
  }, [id]);

  return (
    <div>
      <Sidebar />
      <div className="card pt-3">
        <div className="card-body">
          <form className="text-center mb-5">
            <h1>Update Data Karyawan</h1>
            <div className="mb-3">
              <label className="form-label">
                <b>Nama Karyawan</b>
              </label>
              <input
                type="text"
                className="form-control"
                placeholder="Nama Karyawan"
                onChange={(e) => setNama(e.target.value)}
              />
            </div>
            <div className="mb-3">
              <label className="form-label">
                <b>Tempat lahir</b>
              </label>
              <input
                type="text"
                className="form-control"
                placeholder="Tempat lahir"
                onChange={(e) => setTempat(e.target.value)}
              />
            </div>
            <div className="mb-3">
              <label className="form-label">
                <b>Tanggal lahir</b>
              </label>
              <input
                type="date"
                className="form-control"
                onChange={(e) => setTanggal(e.target.value)}
              />
            </div>
            <div className="mb-3">
              <label className="form-label">
                <b>Alamat</b>
              </label>
              <input
                type="text"
                className="form-control"
                placeholder="Alamat"
                onChange={(e) => setAlamat(e.target.value)}
              />
            </div>
            <div className="field mt-3">
              <button onClick={save} className="btn btn-purple" type="button">
                Update
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
}

export default EditKaryawan;
