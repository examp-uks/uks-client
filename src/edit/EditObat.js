import React, { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import Swal from "sweetalert2";
import { instance as axios } from "../util/Api";
import Sidebar from "../component/Sidebar";
import "../css/button.css";

function EditObat() {
  const navigate = useNavigate();
  const { id } = useParams();

  //   Deklarasi variabel
  const [obat, setObat] = useState("");
  const [stock, setStock] = useState(" ");
  const [expired, setExpired] = useState(" ");

  // Method get
  const updateObat = async () => {
    try {
      // untuk mengirimkan file multipart ke server
      const data = {
        obat: obat,
        stock: stock,
        expired: expired,
      };

      Swal.fire({
        title: "Apakah Anda ingin menyimpan perubahan?",
        icon: "question",
        showDenyButton: true,
        confirmButtonText: "Save",
        denyButtonText: `Cancel`,
      }).then((result) => {
        if (result.isConfirmed) {
          // console.log(data);
          axios.put(`obat/data/update/${id}`, data);

          // console.log(data);
          axios.put(`obat/data/update/${id}`, data);

          navigate("/obat");
          Swal.fire({
            icon: "success",
            title: "Saved!",
            text: "Menu berhasil diubah!",
            showConfirmButton: false,
            timer: 1000,
          });
        } else if (result.isDenied) {
          Swal.fire({
            icon: "info",
            title: "",
            text: "Perubahan tidak disimpan!",
            showConfirmButton: false,
            timer: 1000,
          });
        }
      });
    } catch (err) {
      console.log(err);
    }
  };

  // Method save
  const save = (event) => {
    updateObat();
  };

  // Method get data by id
  const getDataById = async () => {
    const { data } = await axios.get(`obat/data/${id}`);

    setObat(data.obat);
    setStock(data.stock);
    setExpired(data.expired);
  };

  useEffect(() => {
    getDataById();
  }, [id]);

  return (
    <div>
      <Sidebar />
      <div className="card pt-3">
        <div className="card-body">
          <form className="text-center mb-5">
            <h1>Update Daftar Obat</h1>
            <div className="field mt-3">
              <label className="label">Penyakit: </label>
              <input
                type="text"
                className="form-control"
                defaultValue={obat}
                onChange={(e) => setObat(e.target.value)}
              />
            </div>
            <div className="mb-3">
              <label className="form-label">Stock: </label>
              <input
                type="number"
                className="form-control"
                defaultValue={stock}
                onChange={(e) => setStock(e.target.value)}
              />
            </div>
            <div className="mb-3">
              <label className="form-label">Tanggal Expired: </label>
              <input
                type="date"
                className="form-control"
                defaultValue={expired}
                onChange={(e) => setExpired(e.target.value)}
              />
            </div>

            <div className="field mt-3">
              <button onClick={save} className="btn btn-purple" type="button">
                Update
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
}

export default EditObat;
