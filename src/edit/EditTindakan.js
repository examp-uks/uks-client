import React, { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import Swal from "sweetalert2";
import { instance as axios } from "../util/Api";
import Sidebar from "../component/Sidebar";

function EditTindakan() {
  const navigate = useNavigate();
  const { id } = useParams();

  //   Deklarasi variabel
  const [tindakan, setTindakan] = useState("");

  // Method update
  const updateTindakan = async () => {
    try {
      // untuk mengirimkan file multipart ke server
      const data = {
        tindakan: tindakan,
      };

      Swal.fire({
        title: "Apakah Anda ingin menyimpan perubahan?",
        icon: "question",
        showDenyButton: true,
        confirmButtonText: "Save",
        denyButtonText: `Cancel`,
      }).then((result) => {
        if (result.isConfirmed) {
          console.log(data);
          axios.put(`tindakan/data/update/${id}`, data);

          // console.log(data);
          axios.put(`tindakan/data/update/${id}`, data);

          navigate("/tindakan");
          Swal.fire({
            icon: "success",
            title: "Saved!",
            text: "Menu berhasil diubah!",
            showConfirmButton: false,
            timer: 1000,
          });
        } else if (result.isDenied) {
          Swal.fire({
            icon: "info",
            title: "",
            text: "Perubahan tidak disimpan!",
            showConfirmButton: false,
            timer: 1000,
          });
        }
      });
    } catch (err) {
      console.log(err);
    }
  };

  // Method save
  const save = () => {
    updateTindakan();
  };

  // Method get data per id
  const getById = async () => {
    const { data } = await axios.get(`tindakan/data/${id}`);

    setTindakan(data.tindakan);
    console.log(data.tindakan);
  };

  useEffect(() => {
    getById();
  }, [id]);

  return (
    <div>
      <Sidebar />
      <div className="card pt-3">
        <div className="card-body">
          <form className="text-center mb-5">
            <h1>Update Tindakan</h1>
            <div className="field mt-3">
              <label className="label">Tindakan: </label>
              <input
                type="text"
                className="form-control"
                defaultValue={tindakan}
                onChange={(e) => setTindakan(e.target.value)}
              />
            </div>
            <div className="field mt-3">
              <button onClick={save} className="btn btn-purple" type="button">
                Update
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
}

export default EditTindakan;
