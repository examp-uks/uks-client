import React, { useEffect, useState } from "react";
import Sidebar from "../component/Sidebar";
import { useNavigate, useParams } from "react-router-dom";
import { instance as axios } from "../util/Api";
import { storage } from "../Firebase";
import {
  deleteObject,
  getDownloadURL,
  ref,
  uploadBytes,
} from "firebase/storage";
import Swal from "sweetalert2";

function EditProfile() {
  // Deklarasi variabel
  const navigate = useNavigate();
  const [profile, setProfile] = useState("");
  const [editProfile, seteditProfile] = useState();
  const [username, setUsername] = useState(localStorage.getItem("username"));

  const id = localStorage.getItem("id");

  // Method update profile
  const updateProfil = async (downloadURL) => {
    try {
      const data = {
        username: setUsername,
        profile: downloadURL,
      };
      axios.put(`update/${id}`, data);
      const storageRef = ref(storage, `images/${editProfile.profile}`);

      // deleteObject(storageRef);

      Swal.fire("Berhasil menambahkan profil");
    } catch (err) {
      console.log(err);
    }
    navigate(`/`);
  };

  const save = () => {
    // untuk storage
    const storageRef = ref(storage, `images/${profile.name}`);
    // 'file' comes from the Blob or File API
    uploadBytes(storageRef, profile)
      .then((snapshot) => {
        console.log("Upload berhasil");
        console.log(snapshot);
      })
      .catch((err) => {
        console.log(err);
      })
      .finally(() => {
        getDownloadURL(storageRef).then((downloadURL) => {
          // url nya ini nanti untuk dikirim ke server API yang dimasukkan ke database
          updateProfil(downloadURL);
          console.log(downloadURL);
        });
      });
  };

  const getById = async () => {
    const { data } = await axios.get(`/user/${id}`);
    setUsername(data.username);
    setProfile(data.profile);
    seteditProfile(data);
  };

  useEffect(() => {
    getById();
  }, [id]);

  return (
    <div>
      <Sidebar />

      <div className="col-6 col-md-4">
        <div className="card">
          <img src={profile} title="" alt="" className="p-5" />
          <p className="text-center">{username}</p>
          <button
            type="button"
            className="btn btn-purple m-2"
            data-bs-toggle="modal"
            data-bs-target="#exampleModal"
          >
            Edit Profil
          </button>

          <div
            className="modal fade"
            id="exampleModal"
            tabIndex="-1"
            aria-labelledby="exampleModalLabel"
            aria-hidden="true"
          >
            <div className="modal-dialog">
              <div className="modal-content">
                <div className="modal-header">
                  <h1 className="modal-title fs-5" id="exampleModalLabel">
                    Edit Profil
                  </h1>
                  <button
                    type="button"
                    className="btn-close"
                    data-bs-dismiss="modal"
                    aria-label="Close"
                  ></button>
                </div>
                <div className="modal-body">
                  <form>
                    <div className="col-12">
                      <input
                        type="file"
                        className="form-control"
                        placeholder="Edit Profil"
                        accept="image/png, image/jpeg, image/jpg"
                        onChange={(e) => setProfile(e.target.files[0])}
                      />
                    </div>
                  </form>
                </div>
                <div className="modal-footer">
                  <button
                    type="button"
                    className="btn btn-blue"
                    data-bs-dismiss="modal"
                  >
                    Batal
                  </button>
                  <div className="ms-2 me-2">||</div>
                  <button className="btn btn-purple" onClick={save}>
                    Simpan
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default EditProfile;
