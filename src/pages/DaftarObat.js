import React, { useEffect, useState } from "react";
import { instance as axios } from "../util/Api";
import "../css/DaftarObat.css";
import AddObat from "../component/AddObat";
import Swal from "sweetalert2";
import Sidebar from "../component/Sidebar";
import { Link } from "react-router-dom";

function DaftarObat() {
  const [obat, setObat] = useState([]);

  // Method get
  const GetObat = async () => {
    try {
      const { data, status } = await axios.get(`obat/data`);
      if (status === 200) {
        setObat(data);
        // console.log(data);
      }
    } catch (err) {
      console.log(err);
    }
  };

  // Method delete
  const remove = async (id) => {
    await Swal.fire({
      title: "Do You Want to Delete?",
      icon: "warning",
      showDenyButton: true,
      confirmButtonText: "Delete",
      denyButtonText: "Cancel",
    }).then((result) => {
      if (result.isConfirmed) {
        axios.delete(`obat/data/delete/${id}`, {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
        });
        Swal.fire({
          icon: "success",
          title: "Deleted!",
          text: "Successfully deleted your post",
          showConfirmButton: false,
          timer: 1000,
        });
      } else if (result.isDenied) {
        Swal.fire({
          icon: "error",
          title: "Canceled",
          text: "",
          showConfirmButton: false,
          timer: 1000,
        });
      }
    });
  };

  useEffect(() => {
    GetObat();
  }, []);

  return (
    <div className="container">
      <Sidebar />
      <div className="card-daftar">
        <div className="card">
          <div className="card-header d-flex justify-content-between">
            <h4 className="text-light">Daftar Obat</h4>
            <AddObat />
          </div>
          <div className="card-body">
            <div className="card-title">
              <table className="table table-hover">
                <thead>
                  <tr>
                    <th scope="col">No</th>
                    <th scope="col">Nama Obat</th>
                    <th scope="col">Stock</th>
                    <th scope="col">Aksi</th>
                  </tr>
                </thead>
                {obat.map((obat, i) => (
                  <tbody key={i}>
                    <tr>
                      <th scope="row">{i + 1}.</th>
                      <td>{obat.obat}</td>
                      <td>{obat.stock}</td>
                      <td>
                        <Link
                          to={`/updateObat/${obat.id}`}
                          className="btn btn-primary"
                        >
                          <i className="fa-solid fa-pen-to-square fa-xl"></i>
                        </Link>
                        {"  "}
                        <Link
                          onClick={() => remove(obat.id)}
                          className="btn btn-danger"
                        >
                          <i className="fa-solid fa-trash fa-xl"></i>
                        </Link>
                      </td>
                    </tr>
                  </tbody>
                ))}
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default DaftarObat;
