import React from "react";
import Sidebar from "../component/Sidebar";
import "../css/welcome.css";

function Welcome() {
  return (
    <div className="container">
      <Sidebar />
      <div className="row gx-2">
        <div className="col welcome-text">
          <b>
            <h1 className="text-purple">Selamat Datang di UKS,</h1>
          </b>
          <p>
            Selamat datang kembali di sistem aplikasi UKS!!
            <br />
            Kami senang melihat Anda lagi dan berharap Anda terus menikmati dan
            menggunakan kami aplikasi.
          </p>
          <p>SMK BINA NUSANTARA</p>
        </div>
        <div className="col-4">
          <img
            src="https://img.freepik.com/free-vector/boy-science-gown-writing-isolated-background_1308-70280.jpg?w=360&t=st=1684306184~exp=1684306784~hmac=21c57576cbfc7f6bc9e5e7cd0cbe38c6d053d4a8fa1adad64164552946cadad6"
            alt=""
            style={{ width: "150px", height: "350px" }}
          />
        </div>
      </div>
      <div className="row pt-5">
        <div className="col">
          <div className="card">
            <div className="card-body">
              <p className="text">
                "Hal yang paling menyenangkan di tengah masa sulit adalah
                kesehatan yang baik." - Knute Nelson
              </p>
            </div>
          </div>
        </div>
        <div className="col">
          <div className="card">
            <div className="card-body">
              <p className="text">
                “Kesehatanlah yang merupakan kekayaan sejati, bukan kepingan
                emas dan perak.” - Mahatma Gandhi
              </p>
            </div>
          </div>
        </div>
        <div className="col">
          <div className="card">
            <div className="card-body">
              <p className="text">
                "Jauh lebih sulit untuk membuat orang sehat daripada membuat
                mereka sakit." - DeForest Clinton Jarvis
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Welcome;
