import React, { useEffect, useState } from "react";
import { instance as axios } from "../util/Api";
import AddDiagnosa from "../component/AddDiagnosa";
import Swal from "sweetalert2";
import { useParams } from "react-router-dom";
import Sidebar from "../component/Sidebar";
import { Link } from "react-router-dom";

function Diagnosa() {
  const [penyakit, setPenyakit] = useState([]);

  // Method get
  const GetPenyakit = async () => {
    try {
      const { data, status } = await axios.get(`penyakit/data`);
      if (status === 200) {
        setPenyakit(data);
        // console.log(data);
      }
    } catch (err) {
      console.log(err);
    }
  };

  // Method delete
  const remove = async (id) => {
    await Swal.fire({
      title: "Do You Want to Delete?",
      icon: "warning",
      showDenyButton: true,
      confirmButtonText: "Delete",
      denyButtonText: "Cancel",
    }).then((result) => {
      if (result.isConfirmed) {
        axios.delete(`penyakit/data/delete/${id}`, {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
        });
        Swal.fire({
          icon: "success",
          title: "Deleted!",
          text: "Successfully deleted your ",
          showConfirmButton: false,
          timer: 1000,
        });
      } else if (result.isDenied) {
        Swal.fire({
          icon: "error",
          title: "Canceled",
          text: "",
          showConfirmButton: false,
          timer: 1000,
        });
      }
    });
  };

  useEffect(() => {
    GetPenyakit();
  }, []);

  return (
    <div className="container">
      <Sidebar />
      <div className="card-daftar">
        <div className="card">
          <div className="card-header d-flex justify-content-between">
            <h4 className="text-light">Daftar Diagnosa</h4>
            <AddDiagnosa />
          </div>
          <div className="card-body">
            <div className="card-title">
              <table className="table table-hover">
                <thead>
                  <tr>
                    <th scope="col">No</th>
                    <th scope="col">Nama Diagnosa</th>
                    <th scope="col">Aksi</th>
                  </tr>
                </thead>
                {penyakit.map((penyakit, i) => (
                  <tbody key={i}>
                    <tr>
                      <th scope="row">{i + 1}.</th>
                      <td>{penyakit.penyakit}</td>
                      <td>
                        <Link
                          to={`/updatePenyakit/${penyakit.id}`}
                          className="btn btn-primary"
                        >
                          <i className="fa-solid fa-pen-to-square fa-xl"></i>
                        </Link>
                        {"  "}
                        <Link
                          onClick={() => remove(penyakit.id)}
                          className="btn btn-danger"
                        >
                          <i className="fa-solid fa-trash fa-xl"></i>
                        </Link>
                      </td>
                    </tr>
                  </tbody>
                ))}
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Diagnosa;
