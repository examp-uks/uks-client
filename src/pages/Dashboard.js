import React from "react";
import CardDashboard from "../component/CardDashboard";
import Sidebar from "../component/Sidebar";

function Dashboard() {
  return (
    <div className="container pt-3">
      <Sidebar />
      <CardDashboard />
      <div className="card-daftar">
        <div className="card">
          <div className="card-header">
            <h4 className="text-light text-center">Riwayat Pasien</h4>
          </div>
          <div className="card-body">
            <div className="card-title">
              <table className="table table-hover">
                <thead>
                  <tr>
                    <th scope="col">No</th>
                    <th scope="col">Nama Pasien</th>
                    <th scope="col">Status Pasien</th>
                    <th scope="col">Tanggal/Jam Periksa</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <th scope="row">1</th>
                    <td>2</td>
                    <td>3</td>
                    <td>4</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Dashboard;
