import React, { useEffect, useState } from "react";
import AddGuru from "../component/AddGuru";
import Swal from "sweetalert2";
import { instance as axios } from "../util/Api";
import { useParams } from "react-router-dom";
import Sidebar from "../component/Sidebar";
import { Link } from "react-router-dom";

function DataGuru() {
  const [guru, setGuru] = useState([]);

  // Method get
  const GetGuru = async () => {
    try {
      const { data, status } = await axios.get(`guru/data`);
      if (status === 200) {
        setGuru(data);
        // console.log(data);
      }
    } catch (err) {
      console.log(err);
    }
  };

  // Method delete
  const remove = async (id) => {
    await Swal.fire({
      title: "Do You Want to Delete?",
      icon: "warning",
      showDenyButton: true,
      confirmButtonText: "Delete",
      denyButtonText: "Cancel",
    }).then((result) => {
      if (result.isConfirmed) {
        axios.delete(`guru/data/delete/${id}`, {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
        });
        Swal.fire({
          icon: "success",
          title: "Deleted!",
          text: "Successfully deleted your post",
          showConfirmButton: false,
          timer: 1000,
        });
      } else if (result.isDenied) {
        Swal.fire({
          icon: "error",
          title: "Canceled",
          text: "",
          showConfirmButton: false,
          timer: 1000,
        });
      }
    });
  };

  useEffect(() => {
    GetGuru();
  }, []);

  return (
    <div className="container">
      <Sidebar />
      <div className="card-daftar">
        <div className="card">
          <div className="card-header d-flex justify-content-between">
            <h4 className="text-light">Daftar Guru</h4>
            <AddGuru />
          </div>
          <div className="card-body">
            <div className="card-title">
              <table className="table table-hover">
                <thead>
                  <tr>
                    <th scope="col">No</th>
                    <th scope="col">Nama Guru</th>
                    <th scope="col">Tempat/Tanggal Lahir</th>
                    <th scope="col">Alamat</th>
                    <th scope="col">Aksi</th>
                  </tr>
                </thead>
                {guru.map((guru, i) => (
                  <tbody key={i}>
                    <tr>
                      <th scope="row">{i + 1}.</th>
                      <td>{guru.nama}</td>
                      <td>{guru.tanggal}</td>
                      <td>{guru.alamat}</td>
                      <td>
                        <Link
                          to={`/updateGuru/${guru.id}`}
                          className="btn btn-primary"
                        >
                          <i className="fa-solid fa-pen-to-square fa-xl"></i>
                        </Link>
                        {"  "}
                        <Link
                          onClick={() => remove(guru.id)}
                          className="btn btn-danger"
                        >
                          <i className="fa-solid fa-trash fa-xl"></i>
                        </Link>
                      </td>
                    </tr>
                  </tbody>
                ))}
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default DataGuru;
