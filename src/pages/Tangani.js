import React, { useEffect, useState } from "react";
import Sidebar from "../component/Sidebar";
import { instance as axios } from "../util/Api";
import Swal from "sweetalert2";
import { useNavigate } from "react-router-dom";

function Tangani() {
  const navigate = useNavigate();
  const [penyakit, setPenyakit] = useState([]);
  const [penanganan, setPenanganan] = useState([]);
  const [tindakan, setTindakan] = useState([]);
  const [selectPenyakit, setSelectPenyakit] = useState("");
  const [selectPenanganan, setSelectPenanganan] = useState("");
  const [selectTindakan, setSelectTindakan] = useState("");
  const [catatan, setCatatan] = useState("");

  // Method post
  const addTangani = async () => {
    try {
      // untuk mengirimkan file multipart ke server
      const formData = {
        penyakit: { id: penyakit },
        penanganan: { id: penanganan },
        tindakan: { id: tindakan },
        catatan: { id: catatan },
      };

      await axios.post(`pasien/data/add`, formData);
      Swal.fire({
        icon: "success",
        title: "Good job!",
        text: "Category added successfully",
        showConfirmButton: false,
        timer: 1000,
      });
      navigate("/periksaPasien");
    } catch (err) {
      console.log(err);
    }
  };

  // Method get penyakit
  const GetPenyakit = async () => {
    try {
      const { data, status } = await axios.get(`penyakit/data`);
      if (status === 200) {
        setPenyakit(data);
        // console.log(data);
      }
    } catch (err) {
      console.log(err);
    }
  };

  // Method get penanganan
  const GetPenanganan = async () => {
    try {
      const { data, status } = await axios.get(`penanganan/data`);
      if (status === 200) {
        setPenanganan(data);
        // console.log(data);
      }
    } catch (err) {
      console.log(err);
    }
  };

  // Method get tindakan
  const GetTindakan = async () => {
    try {
      const { data, status } = await axios.get(`tindakan/data`);
      if (status === 200) {
        setTindakan(data);
        // console.log(data);
      }
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    GetPenyakit();
    GetPenanganan();
    GetTindakan();
  }, []);

  // Method save
  const save = (e) => {
    e.preventDefault();
    addTangani();
  };

  return (
    <div>
      <Sidebar />
      <div className="card-daftar">
        <div className="card">
          <div className="card-header">
            <p className="text-light text-center">Periksa pasien: nama</p>
          </div>
          <div className="card-body">
            <div className="card-title">
              <form>
                <div className="row">
                  <div className="mb-3 col">
                    <label className="form-label">Nama Pasien</label>
                    <p className="form-control">haii</p>
                  </div>
                  <div className="mb-3 col">
                    <label className="form-label">Status Pasien</label>
                    <p className="form-control">haii</p>
                  </div>
                </div>
                <div className="mb-3">
                  <label className="form-label">Keluhan Pasien</label>
                  <p className="form-control">haii</p>
                </div>
                <div className="row">
                  <div className="col">
                    <label className="form-label">Penyakit Pasien</label>
                    <select
                      className="form-select  form-control-lg "
                      aria-label="Default select example"
                      defaultValue={selectPenyakit}
                      onChange={(e) => {
                        setSelectPenyakit(e.target.value.toString());
                      }}
                      required
                    >
                      <option value="selected">Pilih penyakit</option>
                      {penyakit.map((pen, index) => (
                        <option value={pen.id} key={index}>
                          {pen.penyakit}
                        </option>
                      ))}
                    </select>
                  </div>
                  <div className="col">
                    <label className="form-label">Penanganan Pertama</label>
                    <select
                      className="form-select  form-control-lg "
                      aria-label="Default select example"
                      defaultValue={selectPenanganan}
                      onChange={(e) => {
                        setSelectPenanganan(e.target.value.toString());
                      }}
                      required
                    >
                      <option value="selected">Pilih Penanganan</option>
                      {penanganan.map((pen, index) => (
                        <option value={pen.id} key={index}>
                          {pen.penanganan}
                        </option>
                      ))}
                    </select>
                  </div>
                  <div className="col">
                    <label className="form-label">Tindakan</label>
                    <select
                      className="form-select  form-control-lg "
                      aria-label="Default select example"
                      defaultValue={selectTindakan}
                      onChange={(e) => {
                        setSelectTindakan(e.target.value.toString());
                      }}
                      required
                    >
                      <option value="selected">Pilih Tindakan</option>
                      {tindakan.map((pen, index) => (
                        <option value={pen.id} key={index}>
                          {pen.tindakan}
                        </option>
                      ))}
                    </select>
                  </div>
                  <div className="col">
                    <label className="form-label">Catatan</label>
                    <input
                      type="text"
                      className="form-control"
                      placeholder="Catatan"
                    />
                  </div>
                </div>
                <br />
                <button
                  type="submit"
                  className="btn btn-primary"
                  onClick={save}
                >
                  Submit
                </button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Tangani;
