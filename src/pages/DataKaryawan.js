import React, { useEffect, useState } from "react";
import AddKaryawan from "../component/AddKaryawan";
import Swal from "sweetalert2";
import { instance as axios } from "../util/Api";
import { useParams } from "react-router-dom";
import Sidebar from "../component/Sidebar";
import { Link } from "react-router-dom";

function DataKaryawan() {
  const [karyawan, setKaryawan] = useState([]);

  // Method get
  const GetKaryawan = async () => {
    try {
      const { data, status } = await axios.get(`karyawan/data`);
      if (status === 200) {
        setKaryawan(data);
        // console.log(data);
      }
    } catch (err) {
      console.log(err);
    }
  };

  // Method delete
  const remove = async (id) => {
    await Swal.fire({
      title: "Do You Want to Delete?",
      icon: "warning",
      showDenyButton: true,
      confirmButtonText: "Delete",
      denyButtonText: "Cancel",
    }).then((result) => {
      if (result.isConfirmed) {
        axios.delete(`karyawan/data/delete/${id}`, {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
        });
        Swal.fire({
          icon: "success",
          title: "Deleted!",
          text: "Successfully deleted your post",
          showConfirmButton: false,
          timer: 1000,
        });
      } else if (result.isDenied) {
        Swal.fire({
          icon: "error",
          title: "Canceled",
          text: "",
          showConfirmButton: false,
          timer: 1000,
        });
      }
    });
  };

  useEffect(() => {
    GetKaryawan();
  }, []);

  return (
    <div className="container">
      <Sidebar />
      <div className="card-daftar">
        <div className="card">
          <div className="card-header d-flex justify-content-between">
            <h4 className="text-light">Daftar Karyawan</h4>
            <AddKaryawan />
          </div>
          <div className="card-body">
            <div className="card-title">
              <table className="table table-hover">
                <thead>
                  <tr>
                    <th scope="col">No</th>
                    <th scope="col">Nama Guru</th>
                    <th scope="col">Tempat/Tanggal Lahir</th>
                    <th scope="col">Alamat</th>
                    <th scope="col">Aksi</th>
                  </tr>
                </thead>
                {karyawan.map((karyawan, i) => (
                  <tbody key={i}>
                    <tr>
                      <th scope="row">{karyawan.id}</th>
                      <td>{karyawan.nama}</td>
                      <td>{karyawan.tanggal}</td>
                      <td>{karyawan.alamat}</td>
                      <td>
                        <Link
                          to={`/updateKaryawan/${karyawan.id}`}
                          className="btn btn-primary"
                        >
                          <i className="fa-solid fa-pen-to-square fa-xl"></i>
                        </Link>
                        {"  "}
                        <Link
                          onClick={() => remove(karyawan.id)}
                          className="btn btn-danger"
                        >
                          <i className="fa-solid fa-trash fa-xl"></i>
                        </Link>
                      </td>
                    </tr>
                  </tbody>
                ))}
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default DataKaryawan;
