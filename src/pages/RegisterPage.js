import React, { useState } from "react";
import { Link, useLocation, useNavigate } from "react-router-dom";
import "../css/Account.css";

function RegisterPage() {
  const location = useLocation();
  const navigate = useNavigate();

  const [userRegister, setUserRegister] = useState({
    username: "",
    password: "",
    role: "",
  });

  const handleOnChange = (e) => {
    setUserRegister((currUser) => {
      return { ...currUser, [e.target.id]: e.target.value };
    });
  };

  // Method pos register
  const register = async (e) => {
    e.preventDefault();

    try {
      const response = await fetch(`http://localhost:8080/Register`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(userRegister),
      });

      if (response.ok) {
        const data = await response.json();
        localStorage.setItem("token", data.token);

        if (location.state) {
          navigate(`${location.state.from.pathname}`);
        } else {
          navigate("/Login");
        }
      }
    } catch (err) {
      console.log(err);
    }
  };

  return (
    <div>
      <div className="login-page">
        <h2 className="text-center">Sistem Aplikasi UKS</h2>
        <img
          src="https://binusasmg.sch.id/ppdb/logobinusa.png"
          alt=""
          className="img"
        />
        <div className="form">
          <h4 className="text-center">Register</h4>
          <form className="login-form">
            <p>Email</p>
            <input
              id="username"
              type="text"
              className="form-control"
              placeholder="Username"
              onChange={handleOnChange}
              defaultValue={userRegister.username}
            />
            <p>Password</p>
            <input
              id="password"
              type="password"
              className="form-control"
              placeholder="Password"
              onChange={handleOnChange}
              defaultValue={userRegister.password}
            />
            <p>Role</p>
            <input
              id="role"
              type="text"
              className="form-control"
              placeholder="Role"
              onChange={handleOnChange}
              defaultValue={userRegister.role}
            />
            <button className="btn btn-outline-success" onClick={register}>
              Register
            </button>
            <p className="message">
              Sudah memiliki akun? <Link to="/Login">Log in</Link>
            </p>
          </form>
        </div>
      </div>
    </div>
  );
}

export default RegisterPage;
