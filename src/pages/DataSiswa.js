import React, { useEffect, useState } from "react";
import AddSiswa from "../component/AddSiswa";
import { useParams } from "react-router-dom";
import Swal from "sweetalert2";
import { instance as axios } from "../util/Api";
import Sidebar from "../component/Sidebar";
import { Link } from "react-router-dom";

function DataSiswa() {
  const [siswa, setSiswa] = useState([]);

  // Method get
  const GetSiswa = async () => {
    try {
      const { data, status } = await axios.get(`siswa/data`);
      if (status === 200) {
        setSiswa(data);
        // console.log(data);
      }
    } catch (err) {
      console.log(err);
    }
  };

  // Method delete
  const remove = async (id) => {
    await Swal.fire({
      title: "Do You Want to Delete?",
      icon: "warning",
      showDenyButton: true,
      confirmButtonText: "Delete",
      denyButtonText: "Cancel",
    }).then((result) => {
      if (result.isConfirmed) {
        axios.delete(`siswa/data/delete/${id}`, {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
        });
        Swal.fire({
          icon: "success",
          title: "Deleted!",
          text: "Successfully deleted your post",
          showConfirmButton: false,
          timer: 1000,
        });
      } else if (result.isDenied) {
        Swal.fire({
          icon: "error",
          title: "Canceled",
          text: "",
          showConfirmButton: false,
          timer: 1000,
        });
      }
    });
  };

  useEffect(() => {
    GetSiswa();
  }, []);

  return (
    <div className="container">
      <Sidebar />
      <div className="card-daftar">
        <div className="card">
          <div className="card-header d-flex justify-content-between">
            <h4 className="text-light">Daftar Siswa</h4>
            <AddSiswa />
          </div>
          <div className="card-body">
            <div className="card-title">
              <table className="table table-hover">
                <thead>
                  <tr>
                    <th scope="col">No</th>
                    <th scope="col">Nama Siswa</th>
                    <th scope="col">Kelas</th>
                    <th scope="col">Tempat/Tanggal Lahir</th>
                    <th scope="col">Alamat</th>
                    <th scope="col">Aksi</th>
                  </tr>
                </thead>
                {siswa.map((siswa, i) => (
                  <tbody key={i}>
                    <tr>
                      <th scope="row">{i + 1}.</th>
                      <td>{siswa.nama}</td>
                      <td>{siswa.kelas}</td>
                      <td>{siswa.tanggal}</td>
                      <td>{siswa.alamat}</td>
                      <td>
                        <Link
                          to={`/updateSiswa/${siswa.id}`}
                          className="btn btn-primary"
                        >
                          <i className="fa-solid fa-pen-to-square fa-xl"></i>
                        </Link>
                        {"  "}
                        <Link
                          onClick={() => remove(siswa.id)}
                          className="btn btn-danger"
                        >
                          <i className="fa-solid fa-trash fa-xl"></i>
                        </Link>
                      </td>
                    </tr>
                  </tbody>
                ))}
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default DataSiswa;
