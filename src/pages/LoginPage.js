import React, { useState } from "react";
import { Link, useLocation, useNavigate } from "react-router-dom";
import "../css/Account.css";
import Swal from "sweetalert2";

function LoginPage() {
  const location = useLocation();
  const navigate = useNavigate();

  const [userLogin, setUserLogin] = useState({
    username: "",
    password: "",
  });

  const handleOnChange = (e) => {
    setUserLogin((currUser) => {
      return { ...currUser, [e.target.id]: e.target.value };
    });
  };

  // Method post login
  const login = async (e) => {
    e.preventDefault();

    try {
      const response = await fetch(`http://localhost:8080/login`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(userLogin),
      });

      if (response.ok) {
        const data = await response.json();
        localStorage.setItem("token", data.token);
        localStorage.setItem("id", data.userData.id);
        localStorage.setItem("username", data.userData.username);
        localStorage.setItem("role", data.userData.role);
        localStorage.setItem("profile", data.userData.profile);

        // console.log(data.userData.id);
        // console.log(data.userData.username);

        if (location.state) {
          navigate(`${location.state.from.pathname}`);
        } else {
          navigate("/");
          Swal.fire({
            icon: "success",
            title: "Selamat Login Sukses",
            showConfirmButton: false,
            timer: 800,
          });
        }
      } else {
        Swal.fire({
          icon: "error",
          title: "Username Atau Password Tidak Valid",
          showConfirmButton: false,
          timer: 800,
        });
      }
    } catch (err) {
      console.log(err);
    }
  };

  return (
    <div>
      <div className="login-page">
        <h2 className="text-center">Sistem Aplikasi UKS</h2>
        <img
          src="https://binusasmg.sch.id/ppdb/logobinusa.png"
          alt=""
          className="img"
        />
        <div className="login-page">
          <div className="form">
            <form className="login-form">
              <input
                id="username"
                type="text"
                className="form-control"
                placeholder="username"
                onChange={handleOnChange}
                defaultValue={userLogin.username}
              />
              <input
                id="password"
                type="password"
                className="form-control"
                placeholder="password"
                onChange={handleOnChange}
                defaultValue={userLogin.password}
              />
              <button onClick={login} className="btn">
                login
              </button>
              <p className="message">
                Tidak memiliki akun? <Link to="/Register">Buat akun</Link>
              </p>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
}

export default LoginPage;
