import React, { useEffect, useState } from "react";
import AddPenanganan from "../component/AddPenanganan";
import Sidebar from "../component/Sidebar";
import { useParams } from "react-router-dom";
import Swal from "sweetalert2";
import { instance as axios } from "../util/Api";
import { Link } from "react-router-dom";

function Penanganan() {
  const [penanganan, setPenanganan] = useState([]);

  // Method get
  const GetPenanganan = async () => {
    try {
      const { data, status } = await axios.get(`penanganan/data`);
      if (status === 200) {
        setPenanganan(data);
        // console.log(data);
      }
    } catch (err) {
      console.log(err);
    }
  };

  // Method delete
  const remove = async (id) => {
    await Swal.fire({
      title: "Do You Want to Delete?",
      icon: "warning",
      showDenyButton: true,
      confirmButtonText: "Delete",
      denyButtonText: "Cancel",
    }).then((result) => {
      if (result.isConfirmed) {
        axios.delete(`penanganan/data/delete/${id}`, {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
        });
        Swal.fire({
          icon: "success",
          title: "Deleted!",
          text: "Successfully deleted your post",
          showConfirmButton: false,
          timer: 1000,
        });
      } else if (result.isDenied) {
        Swal.fire({
          icon: "error",
          title: "Canceled",
          text: "",
          showConfirmButton: false,
          timer: 1000,
        });
      }
    });
  };

  useEffect(() => {
    GetPenanganan();
  }, []);

  return (
    <div className="container">
      <Sidebar />
      <div className="card-daftar">
        <div className="card">
          <div className="card-header d-flex justify-content-between">
            <h4 className="text-light">Daftar Penanganan Pertama</h4>
            <AddPenanganan />
          </div>
          <div className="card-body">
            <div className="card-title">
              <table className="table table-hover">
                <thead>
                  <tr>
                    <th scope="col">No</th>
                    <th scope="col">Nama Penanganan</th>
                    <th scope="col">Aksi</th>
                  </tr>
                </thead>
                {penanganan.map((penanganan, i) => (
                  <tbody key={i}>
                    <tr>
                      <th scope="row">{i + 1}.</th>
                      <td>{penanganan.penanganan}</td>
                      <td>
                        <Link
                          to={`/updatePenanganan/${penanganan.id}`}
                          className="btn btn-primary"
                        >
                          <i className="fa-solid fa-pen-to-square fa-xl"></i>
                        </Link>
                        {"  "}
                        <Link
                          onClick={() => remove(penanganan.id)}
                          className="btn btn-danger"
                        >
                          <i className="fa-solid fa-trash fa-xl"></i>
                        </Link>
                      </td>
                    </tr>
                  </tbody>
                ))}
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Penanganan;
