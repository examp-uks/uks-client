import React, { useEffect, useState } from "react";
import FilterData from "../component/FilterData";
import AddPasien from "../component/AddPasien";
import Sidebar from "../component/Sidebar";
import { useParams } from "react-router-dom";
import Swal from "sweetalert2";
import { instance as axios } from "../util/Api";
import { Link } from "react-router-dom";

function PeriksaPasien() {
  const [pasien, setPasien] = useState([]);

  // Method get pasien
  const GetPasien = async () => {
    try {
      const { data, status } = await axios.get(`pasien/data`);
      if (status === 200) {
        setPasien(data);
        // console.log(data);
      }
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    GetPasien();
  }, []);

  return (
    <div className="container">
      <Sidebar />
      <div className="card-daftar">
        <div className="card">
          <div className="card-header d-flex justify-content-between">
            <h4 className="text-light">Filter Rekap Data</h4>
            <FilterData />
          </div>
          <div className="card-body">
            <div className="card-title">
              <h1 className="text-center container pt-2">
                <i className="fa-solid fa-circle-exclamation fa-2xl mb-5"></i>
                <p>Filter terlebih dahulu sesuai tanggal yang diinginkan.</p>
              </h1>
            </div>
          </div>
        </div>
      </div>
      <div className="card-daftar">
        <div className="card">
          <div className="card-header d-flex justify-content-between">
            <h4 className="text-light">Daftar Pasien</h4>
            <AddPasien />
          </div>
          <div className="card-body">
            <div className="card-title">
              <table className="table table-hover">
                <thead>
                  <tr>
                    <th scope="col">No</th>
                    <th scope="col">Nama Pasien</th>
                    <th scope="col">Status Pasien</th>
                    <th scope="col">Jabatan</th>
                    <th scope="col">Tanggal/Jam</th>
                    <th scope="col">Keterangan</th>
                    <th scope="col">Status</th>
                    <th scope="col">Aksi</th>
                  </tr>
                </thead>
                {pasien.map((pasien, i) => (
                  <tbody key={i}>
                    <tr>
                      <th scope="row">{i + 1}.</th>
                      <td>{pasien.karyawan.nama}</td>
                      <td>{pasien.status.status}</td>
                      <td>{pasien.status.status}</td>
                      <td>-</td>
                      <td>-</td>
                      <td>Otto</td>
                      <td>
                        <Link
                          className="btn btn-danger"
                          to={`/tangani/${pasien.id}`}
                        >
                          Tangani
                        </Link>
                      </td>
                    </tr>
                  </tbody>
                ))}
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default PeriksaPasien;
