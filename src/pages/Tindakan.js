import React, { useEffect, useState } from "react";
import AddTindakan from "../component/AddTindakan";
import Sidebar from "../component/Sidebar";
import { useParams } from "react-router-dom";
import { instance as axios } from "../util/Api";
import Swal from "sweetalert2";
import { Link } from "react-router-dom";

function Tindakan() {
  const [tindakan, setTindakan] = useState([]);

  // Method get
  const GetTindakan = async () => {
    try {
      const { data, status } = await axios.get(`tindakan/data`);
      if (status === 200) {
        setTindakan(data);
        // console.log(data);
      }
    } catch (err) {
      console.log(err);
    }
  };

  // Method delete
  const remove = async (id) => {
    await Swal.fire({
      title: "Do You Want to Delete?",
      icon: "warning",
      showDenyButton: true,
      confirmButtonText: "Delete",
      denyButtonText: "Cancel",
    }).then((result) => {
      if (result.isConfirmed) {
        axios.delete(`tindakan/data/delete/${id}`, {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
        });
        Swal.fire({
          icon: "success",
          title: "Deleted!",
          text: "Successfully deleted your post",
          showConfirmButton: false,
          timer: 1000,
        });
      } else if (result.isDenied) {
        Swal.fire({
          icon: "error",
          title: "Canceled",
          text: "",
          showConfirmButton: false,
          timer: 1000,
        });
      }
    });
  };

  useEffect(() => {
    GetTindakan();
  }, []);

  return (
    <div className="container">
      <Sidebar />
      <div className="card-daftar">
        <div className="card">
          <div className="card-header d-flex justify-content-between">
            <h4 className="text-light">Daftar Tindakan</h4>
            <AddTindakan />
          </div>
          <div className="card-body">
            <div className="card-title">
              <table className="table table-hover">
                <thead>
                  <tr>
                    <th scope="col">No</th>
                    <th scope="col">Nama Tindakan</th>
                    <th scope="col">Aksi</th>
                  </tr>
                </thead>
                {tindakan.map((tindakan, i) => (
                  <tbody key={i}>
                    <tr>
                      <th scope="row">{i + 1}.</th>
                      <td>{tindakan.tindakan}</td>
                      <td>
                        <Link
                          to={`/updateTindakan/${tindakan.id}`}
                          className="btn btn-primary"
                        >
                          <i className="fa-solid fa-pen-to-square fa-xl"></i>
                        </Link>
                        {"  "}
                        <Link
                          onClick={() => remove(tindakan.id)}
                          className="btn btn-danger"
                        >
                          <i className="fa-solid fa-trash fa-xl"></i>
                        </Link>
                      </td>
                    </tr>
                  </tbody>
                ))}
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Tindakan;
