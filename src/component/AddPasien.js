import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { instance as axios } from "../util/Api";
import Swal from "sweetalert2";

function AddPasien() {
  // Deklarasi variabel
  const navigate = useNavigate();
  const [keluhan, setKeluhan] = useState(" ");
  const [dataPasien, setDataPasien] = useState([]);
  const [status, setStatus] = useState([]);
  const [namaPasien, setNamaPasien] = useState(0);

  // Method post
  const AddPasien = async () => {
    try {
      // untuk mengirimkan file multipart ke server
      const formData = {
        status: { id: status },
        nama: { id: namaPasien },
        keluhan: keluhan,
      };
      console.log(formData);

      await axios.post(`pasien/data/add`, formData);
      Swal.fire({
        icon: "success",
        title: "Good job!",
        text: "Category added successfully",
        showConfirmButton: false,
        timer: 1000,
      });
      navigate("/pasien");
    } catch (err) {
      console.log(err);
    }
  };

  // Get guru
  const GetGuru = async () => {
    try {
      const { data, status } = await axios.get(`guru/data`);
      if (status === 200) {
        setDataPasien(data);
        // console.log(data);
      }
    } catch (err) {
      console.log(err);
    }
  };

  // Get siswa
  const GetSiswa = async () => {
    try {
      const { data, status } = await axios.get(`siswa/data`);
      if (status === 200) {
        setDataPasien(data);
        // console.log(data);
      }
    } catch (err) {
      console.log(err);
    }
  };

  // Get karyawan
  const GetKaryawan = async () => {
    try {
      const { data, status } = await axios.get(`karyawan/data`);
      if (status === 200) {
        setDataPasien(data);
        // console.log(data);
      }
    } catch (err) {
      console.log(err);
    }
  };

  // Method save
  const save = (e) => {
    e.preventDefault();
    AddPasien();
  };

  useEffect(() => {
    GetGuru();
    GetSiswa();
    GetKaryawan();
  }, []);

  // Method Status get
  const changeStatus = (e) => {
    setStatus(e.target.value);

    if (e.target.value === "Siswa") {
      GetSiswa();
    } else if (e.target.value === "Karyawan") {
      GetKaryawan();
    } else if (e.target.value === "Guru") {
      GetGuru();
    }
  };

  return (
    <div>
      {/* Button trigger modal */}
      <button
        type="button"
        className="btn btn-primary"
        data-bs-toggle="modal"
        data-bs-target="#exampleModal2"
        id="AddPasien"
      >
        <i className="fa-solid fa-plus"></i> Tambah
      </button>

      {/* Modal */}
      <div
        className="modal fade"
        id="exampleModal2"
        tabIndex="-1"
        aria-labelledby="exampleModalLabel"
        aria-hidden="true"
      >
        <div className="modal-dialog">
          <div className="modal-content">
            <div className="modal-header">
              <h1 className="modal-title fs-5" id="exampleModalLabel">
                <b>Tambah Daftar Pasien</b>
              </h1>
            </div>
            <div className="modal-body">
              <form>
                <div className="mb-3">
                  <label className="form-label">
                    <b>Status Pasien</b>
                  </label>
                  <select
                    className="form-select"
                    aria-label="Default select example"
                  >
                    <option
                      value="selected"
                      onChange={(e) => changeStatus(e)}
                      required
                    >
                      Pilih Status
                    </option>
                    <option value="Guru">Guru</option>
                    <option value="Siswa">Siswa</option>
                    <option value="Karyawan">Karyawan</option>
                  </select>
                </div>
                <div className="mb-3">
                  <label className="form-label">
                    <b>Nama Pasien</b>
                  </label>
                  <select
                    className="form-select"
                    aria-label="Default select example"
                  >
                    <option
                      value="selected"
                      onChange={(e) => setNamaPasien(e.target.value)}
                      required
                    >
                      Pilih Nama
                    </option>
                    {dataPasien.map((pas, i) => (
                      <option value={pas.id} key={i}>
                        {pas.nama}
                      </option>
                    ))}
                  </select>
                </div>
                <div className="mb-3">
                  <label className="form-label">
                    <b>Keluhan Pasien</b>
                  </label>
                  <input
                    type="text"
                    className="form-control"
                    placeholder="Keluhan Pasien"
                    onChange={(e) => setKeluhan(e.target.value)}
                  />
                </div>
              </form>
            </div>
            <div className="modal-footer">
              <button
                type="button"
                className="btn btn-danger"
                data-bs-dismiss="modal"
              >
                Batal
              </button>
              <button type="button" className="btn btn-primary" onClick={save}>
                Simpan
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default AddPasien;
