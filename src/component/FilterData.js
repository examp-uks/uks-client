import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import { instance as axios } from "../util/Api";
import Swal from "sweetalert2";

function FilterData() {
  // Deklarasi variabel
  const navigate = useNavigate();
  const [from, setFrom] = useState(" ");
  const [to, setTo] = useState(" ");

  const FilterData = () => {
    try {
      // untuk mengirimkan file multipart ke server
      const formData = {
        from: from,
        to: to,
      };
      Swal.fire({
        icon: "success",
        title: "Good job!",
        text: "Category added successfully",
        showConfirmButton: false,
        timer: 1000,
      });
      navigate("/DaftarObat");
    } catch (err) {
      console.log(err);
    }
  };

  const save = (e) => {
    e.preventDefault();
    FilterData();
  };

  return (
    <div>
      {/* Button trigger modal */}
      <button
        type="button"
        className="btn btn-primary"
        data-bs-toggle="modal"
        data-bs-target="#exampleModal"
      >
        Filter Tanggal
      </button>

      {/* Modal */}
      <div
        className="modal fade"
        id="exampleModal"
        tabIndex="-1"
        aria-labelledby="exampleModalLabel"
        aria-hidden="true"
      >
        <div className="modal-dialog">
          <div className="modal-content">
            <div className="modal-header">
              <h1 className="modal-title fs-5" id="exampleModalLabel">
                <b>Filter Rekap Data Pasien</b>
              </h1>
            </div>
            <div className="modal-body">
              <form>
                <div className="mb-3">
                  <label className="form-label">
                    <b>Dari Tanggal</b>
                  </label>
                  <input
                    type="date"
                    className="form-control"
                    onChange={(e) => setFrom(e.target.value)}
                  />
                </div>
                <div className="mb-3">
                  <label className="form-label">
                    <b>Sampai Tanggal</b>
                  </label>
                  <input
                    type="date"
                    className="form-control"
                    onChange={(e) => setTo(e.target.value)}
                  />
                </div>
              </form>
            </div>
            <div className="modal-footer">
              <button
                type="button"
                className="btn btn-danger"
                data-bs-dismiss="modal"
              >
                Batal
              </button>
              <button
                type="button"
                className="btn btn-primary"
                // onClick={save}
              >
                Simpan
              </button>
            </div>
          </div>
        </div>
        <div className="card-body">
          <div className="card-title"></div>
        </div>
      </div>
    </div>
  );
}

export default FilterData;
