import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import { instance as axios } from "../util/Api";
import Swal from "sweetalert2";

function AddObat() {
  // Deklarasi variabel
  const navigate = useNavigate();
  const [obat, setObat] = useState(" ");
  const [stock, setStock] = useState(" ");
  const [expired, setExpired] = useState(" ");

  // Method post
  const AddDiagnosa = async () => {
    try {
      // untuk mengirimkan file multipart ke server
      const formData = {
        obat: obat,
        stock: stock,
        expired: expired,
      };

      await axios.post(`obat/data/add`, formData);
      Swal.fire({
        icon: "success",
        title: "Good job!",
        text: "Added successfully",
        showConfirmButton: false,
        timer: 1000,
      });
      navigate("/obat");
    } catch (err) {
      console.log(err);
    }
  };

  // Method save
  const save = (e) => {
    e.preventDefault();
    AddDiagnosa();
  };

  return (
    <div>
      {/* Button trigger modal */}
      <button
        type="button"
        className="btn btn-primary"
        data-bs-toggle="modal"
        data-bs-target="#exampleModal"
      >
        <i className="fa-solid fa-plus"></i> Tambah
      </button>

      {/* Modal */}
      <div
        className="modal fade"
        id="exampleModal"
        tabIndex="-1"
        aria-labelledby="exampleModalLabel"
        aria-hidden="true"
      >
        <div className="modal-dialog">
          <div className="modal-content">
            <div className="modal-header">
              <h1 className="modal-title fs-5" id="exampleModalLabel">
                <b>Tambah Obat</b>
              </h1>
            </div>
            <div className="modal-body">
              <form>
                <div className="mb-3">
                  <label className="form-label">
                    <b>Nama Obat</b>
                  </label>
                  <input
                    type="text"
                    className="form-control"
                    placeholder="Nama Obat"
                    onChange={(e) => setObat(e.target.value)}
                  />
                </div>
                <div className="mb-3">
                  <label className="form-label">
                    <b>Stock</b>
                  </label>
                  <input
                    type="number"
                    className="form-control"
                    placeholder="Stock"
                    onChange={(e) => setStock(e.target.value)}
                  />
                </div>
                <div className="mb-3">
                  <label className="form-label">
                    <b>Tanggal Expired</b>
                  </label>
                  <input
                    type="date"
                    className="form-control"
                    onChange={(e) => setExpired(e.target.value)}
                  />
                </div>
              </form>
            </div>
            <div className="modal-footer">
              <button
                type="button"
                className="btn btn-danger"
                data-bs-dismiss="modal"
              >
                Batal
              </button>
              <button type="button" className="btn btn-primary" onClick={save}>
                Simpan
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default AddObat;
