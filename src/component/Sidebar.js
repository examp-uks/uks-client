import "../css/Sidebar.css";
import { Link, Navigate, useParams } from "react-router-dom";
import React, { useState, useEffect } from "react";
import DateTime from "./DateTime";
import { instance as axios } from "../util/Api";

function Sidebar() {
  // Deklarasi varabel
  const [currentDate, setCurrentDate] = useState(new Date());
  const [profile, setProfile] = useState(localStorage.getItem("profile"));
  const id = localStorage.getItem("id");

  const [user, setUser] = useState([]);

  useEffect(() => {
    const interval = setInterval(() => {
      setCurrentDate(new Date());
    }, 1000);

    return () => clearInterval(interval);
  }, []);

  // Method get profile
  const GetUser = async () => {
    try {
      const { data, status } = await axios.get(`user/${id}`);
      if (status === 200) {
        setUser(data);
        // console.log(data);
      }
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    GetUser();
  }, []);

  // Method logout
  const logout = () => {
    localStorage.removeItem("token");
    Navigate("/login");
  };

  return (
    <div>
      {/* Sidebar */}
      <div className="sidebar text-light">
        <p className="container">
          <b>
            {" "}
            SISTEM APLIKASI UKS <br /> SMK BINA NUSANTARA
          </b>
        </p>
        <hr className="text-light" />
        <div className="container mb-2">
          <a href="/dashboard">
            <i className="fa-solid fa-house"></i> Dashboard
          </a>
          <a href="/periksaPasien">
            <i className="fa-solid fa-stethoscope"></i> Periksa Pasien
          </a>
          <div className="dropdown">
            <a href="//" data-bs-toggle="dropdown">
              <i className="fa-solid fa-arrow-down"></i> Data
            </a>
            <ul
              className="dropdown-menu"
              style={{ backgroundColor: "rgb(19, 34, 41)" }}
            >
              <li>
                <a className="dropdown-item" href="/guru">
                  Daftar Guru
                </a>
              </li>
              <li>
                <a className="dropdown-item" href="/siswa">
                  Daftar Siswa
                </a>
              </li>
              <li>
                <a className="dropdown-item" href="/karyawan">
                  Daftar Karyawan
                </a>
              </li>
            </ul>
          </div>
          <a href="/diagnosa">
            <i className="fa-solid fa-person-dots-from-line"></i> Diagnosa
          </a>
          <a href="/penanganan">
            <i className="fa-solid fa-hand-holding-medical"></i> Penanganan
            Pertama
          </a>
          <a href="/tindakan">
            <i className="fa-solid fa-location-crosshairs"></i> Tindakan
          </a>
          <a href="/obat">
            <i className="fa-solid fa-capsules"></i> Daftar Obat P3K
          </a>
        </div>
        <DateTime />
        <div className="container position-absolute bottom-0 start-50 translate-middle-x">
          <Link className="nav-link active" to="/login">
            <i className="fa-solid fa-right-from-bracket" onClick={logout}></i>{" "}
            Log Out
          </Link>
        </div>
      </div>
      <div style={{ marginLeft: "90%" }} className="d-flex mb-3 pt-2">
        <h5 className="pt-3 me-2">{localStorage.getItem("username")}</h5>
        <Link to={`/user/${user.id}`}>
          <img
            src={`${localStorage.getItem("profile")}`}
            alt="Logo"
            width="60"
            height="54"
            className="align-content-end"
            style={{ borderRadius: "50%" }}
          />
        </Link>
      </div>
    </div>
  );
}

export default Sidebar;
