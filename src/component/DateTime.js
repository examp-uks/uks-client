import React from "react";

function DateTime() {
  const now = new Date();
  const formattedDate = now.toLocaleDateString();
  const formattedTime = now.toLocaleTimeString();

  return (
    <div className="text-center mt-5">
      <h6>{formattedDate}</h6>
      <h5>{formattedTime}</h5>
    </div>
  );
}

export default DateTime;
