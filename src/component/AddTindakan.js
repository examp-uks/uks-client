import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import { instance as axios } from "../util/Api";
import Swal from "sweetalert2";

function AddTindakan() {
  // Deklarasi variabel
  const navigate = useNavigate();
  const [tindakan, setPenyakit] = useState(" ");

  // Method post
  const AddTindakan = async () => {
    try {
      // untuk mengirimkan file multipart ke server
      const formData = {
        tindakan: tindakan,
      };

      await axios.post(`tindakan/data/add`, formData);
      Swal.fire({
        icon: "success",
        title: "Good job!",
        text: "Category added successfully",
        showConfirmButton: false,
        timer: 1000,
      });
      navigate("/tindakan");
    } catch (err) {
      console.log(err);
    }
  };

  // Method save
  const save = (e) => {
    e.preventDefault();
    AddTindakan();
  };

  return (
    <div>
      <div>
        {/* Button trigger modal */}
        <button
          type="button"
          className="btn btn-primary"
          data-bs-toggle="modal"
          data-bs-target="#exampleModal"
        >
          <i className="fa-solid fa-plus"></i> Tambah
        </button>

        {/* Modal */}
        <div
          className="modal fade"
          id="exampleModal"
          tabIndex="-1"
          aria-labelledby="exampleModalLabel"
          aria-hidden="true"
        >
          <div className="modal-dialog">
            <div className="modal-content">
              <div className="modal-header">
                <h1 className="modal-title fs-5" id="exampleModalLabel">
                  <b>Tambah Tindakan</b>
                </h1>
              </div>
              <div className="modal-body">
                <form>
                  <div className="mb-3">
                    <label className="form-label">
                      <b>Nama Tindakan</b>
                    </label>
                    <input
                      type="text"
                      className="form-control"
                      placeholder="Nama Tindakan"
                      onChange={(e) => setPenyakit(e.target.value)}
                    />
                  </div>
                </form>
              </div>
              <div className="modal-footer">
                <button
                  type="button"
                  className="btn btn-danger"
                  data-bs-dismiss="modal"
                >
                  Batal
                </button>
                <button
                  type="button"
                  className="btn btn-primary"
                  onClick={save}
                >
                  Simpan
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default AddTindakan;
