import React from "react";
import "../css/button.css";

function CardDashboard() {
  return (
    <div className="text-center">
      <div className="row align-items-start">
        <div className="col">
          <div className="card">
            <div className="card-body">
              <h6>
                <b>Daftar Pasien Guru</b>
              </h6>
              <h6
                className="card-subtitle text-center fs-2 mb-2"
                style={{ color: "#714df1" }}
              >
                <span className="fa-stack fa-xs">
                  <i className="fa fa-circle fa-stack-2x"></i>
                  <i className="fa-solid fa-hospital-user fa-stack-1x fa-inverse"></i>
                </span>
                0 Guru
              </h6>
            </div>
          </div>
        </div>
        <div className="col">
          <div className="card">
            <div className="card-body">
              <h6>
                <b>Daftar Pasien Siswa</b>
              </h6>
              <h6
                className="card-subtitle text-center fs-2 mb-2"
                style={{ color: "#714df1" }}
              >
                <span className="fa-stack fa-xs">
                  <i className="fa fa-circle fa-stack-2x"></i>
                  <i className="fa-solid fa-hospital-user fa-stack-1x fa-inverse"></i>
                </span>
                0 Siswa
              </h6>
            </div>
          </div>
        </div>
        <div className="col">
          <div className="card">
            <div className="card-body">
              <h6>
                <b>Daftar Pasien Karyawan</b>
              </h6>
              <h6
                className="card-subtitle text-center fs-2 mb-2"
                style={{ color: "#714df1" }}
              >
                <span className="fa-stack fa-xs">
                  <i className="fa fa-circle fa-stack-2x"></i>
                  <i className="fa-solid fa-hospital-user fa-stack-1x fa-inverse"></i>
                </span>
                0 Karyawan
              </h6>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default CardDashboard;
