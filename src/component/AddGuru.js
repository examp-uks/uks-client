import React, { useState } from "react";
import "../css/button.css";
import { instance as axios } from "../util/Api";
import { useNavigate } from "react-router-dom";
import Swal from "sweetalert2";

function AddGuru() {
  // Deklarasi variabel
  const navigate = useNavigate();
  const [nama, setNama] = useState(" ");
  const [tempat, setTempat] = useState(" ");
  const [tanggal, setTanggal] = useState(" ");
  const [alamat, setAlamat] = useState(" ");

  // Method post
  const AddGuru = async () => {
    try {
      // untuk mengirimkan file multipart ke server
      const formData = {
        nama: nama,
        tempat: tempat,
        tanggal: tanggal,
        alamat: alamat,
      };

      await axios.post(`guru/data/add`, formData);
      Swal.fire({
        icon: "success",
        title: "Good job!",
        text: "Added successfully",
        showConfirmButton: false,
        timer: 1000,
      });
      navigate("/guru");
    } catch (err) {
      console.log(err);
    }
  };

  // Method save
  const save = (e) => {
    e.preventDefault();
    AddGuru();
  };

  return (
    <div>
      <div>
        {/* Button trigger modal */}
        <button
          type="button"
          className="btn btn-primary"
          data-bs-toggle="modal"
          data-bs-target="#exampleModal"
        >
          <i className="fa-solid fa-plus"></i> Tambah
        </button>

        {/* Modal */}
        <div
          className="modal fade"
          id="exampleModal"
          tabIndex="-1"
          aria-labelledby="exampleModalLabel"
          aria-hidden="true"
        >
          <div className="modal-dialog">
            <div className="modal-content">
              <div className="modal-header">
                <h1 className="modal-title fs-5" id="exampleModalLabel">
                  <b>Tambah Guru</b>
                </h1>
              </div>
              <div className="modal-body">
                <form>
                  <div className="mb-3">
                    <label className="form-label">
                      <b>Nama Guru</b>
                    </label>
                    <input
                      type="text"
                      className="form-control"
                      placeholder="Nama Guru"
                      onChange={(e) => setNama(e.target.value)}
                    />
                  </div>
                  <div className="mb-3">
                    <label className="form-label">
                      <b>Tempat lahir</b>
                    </label>
                    <input
                      type="text"
                      className="form-control"
                      placeholder="Tempat lahir"
                      onChange={(e) => setTempat(e.target.value)}
                    />
                  </div>
                  <div className="mb-3">
                    <label className="form-label">
                      <b>Tanggal lahir</b>
                    </label>
                    <input
                      type="date"
                      className="form-control"
                      onChange={(e) => setTanggal(e.target.value)}
                    />
                  </div>
                  <div className="mb-3">
                    <label className="form-label">
                      <b>Alamat</b>
                    </label>
                    <input
                      type="text"
                      className="form-control"
                      placeholder="Alamat"
                      onChange={(e) => setAlamat(e.target.value)}
                    />
                  </div>
                </form>
              </div>
              <div className="modal-footer">
                <button
                  type="button"
                  className="btn btn-blue"
                  data-bs-dismiss="modal"
                >
                  Batal
                </button>
                <button type="button" className="btn btn-purple" onClick={save}>
                  Simpan
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default AddGuru;
